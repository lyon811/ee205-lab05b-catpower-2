/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file CatP.cpp
/// @version 1.0
///
/// @author Lyon Singleton <lyonws@hawaii.edu>
/// @date 16_Feb_2022
/////////////////////////////////////////////////////////////////////////////
//



#include "CatP.h"
#pragma once


double fromCatPowerToJoule( double catPower ) {
   return 0.0;  // Cats do no work
}

double fromJouleToCatPower( double joule ) {
   return 0.0;  // Cats do no work
}