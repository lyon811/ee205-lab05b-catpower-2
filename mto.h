/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file mto.h
/// @version 1.0
///
/// @author Lyon Singleton <lyonws@hawaii.edu>
/// @date 16_Feb_2022
/////////////////////////////////////////////////////////////////////////////







const double MEGATONS_IN_A_JOULE   = 1 / 4.184e15 ;
const char MEGATON   = 'm';



extern double  fromJouleToMegaton( double joule ) ;
extern double  fromMegatonToJoule( double megaton ) ; 