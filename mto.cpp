/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file mto.cpp
/// @version 1.0
///
/// @author Lyon Singleton <lyonws@hawaii.edu>
/// @date 16_Feb_2022
/////////////////////////////////////////////////////////////////////////////
//







#include "mto.h"
#pragma once
double fromMegatonToJoule( double megaton ) {
   return megaton / MEGATONS_IN_A_JOULE ;
}


double fromJouleToMegaton( double joule ) {
   return joule * MEGATONS_IN_A_JOULE ;
}