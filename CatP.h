/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file CatP.h
/// @version 1.0
///
/// @author  Lyon Singleton <lyonws@hawaii.edu>
/// @date 16_Feb_2022
/////////////////////////////////////////////////////////////////////////////
//








const char JOULE      = 'j';
const char CATPOWER  = 'c';
const double CATPOWER_IN_A_JOULE  = 0 ;

extern double fromCatPowerToJoule( double catPower ) ;
extern double  fromJouleToCatPower( double joule ) ;

