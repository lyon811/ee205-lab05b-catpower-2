/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file gge.cpp
/// @version 1.0
///
/// @author Lyon Singleton <lyonws@hawaii.edu>
/// @date 16_Feb_2022
/////////////////////////////////////////////////////////////////////////////
//







#include "gge.h"
#pragma once





double fromGgeToJoule( double gge ) {
   return gge / GGE_IN_A_JOULE ;
}

double fromJouleToGge( double joule ) {
   return joule * GGE_IN_A_JOULE ;
}