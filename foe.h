/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file foe.h
/// @version 1.0
///
/// @author Lyon Singleton <lyonws@hawaii.edu>
/// @date 16_Feb_2022
/////////////////////////////////////////////////////////////////////////////
//









const double FOE_IN_A_JOULE  = 1 / 1.0e24 ;
const char FOE  = 'f';


extern double fromJouleToFoe( double joule ) ;
extern double fromFoeToJoule( double foe ) ; 

